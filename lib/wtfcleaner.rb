#!/usr/bin/ruby
require 'fileutils'

def puts?(text, should_display)
  if should_display
    puts text
  end
end

def remove_matching_subdirs(name, silent=false)
  (Dir['**/*'] + Dir['**/.*'])
    .select { |d| File.directory? d }
    .select { |d| File.basename(d).downcase == name.downcase }
    .each   { |d| puts?("\tRemoving: #{d}", !silent) }
    .each   { |d| FileUtils.rm_rf d }
end

def gentle_clean(gradlew, silent=false)
  puts?('Starting Gradle clean...', !silent)
  `#{gradlew} clean -q`

  puts?('Killing Gradle daemon...', !silent)
  `#{gradlew} --stop`

  puts?('Killing ADB server...', !silent)
  `adb kill-server`

  puts?('Removing all build directories...', !silent)
  remove_matching_subdirs('build')

  puts?('Removing all .gradle directories...', !silent)
  remove_matching_subdirs('.gradle')
end

def nuke_cache(gradlew, silent=false)
  puts?("Nuking all build caches...", !silent)
  `#{gradlew} cleanBuildCache`

  Dir["#{Dir.home}/.*"]
    .select { |d| File.directory? d } 
    .select { |d| File.basename(d).downcase == '.gradle'}
    .map    { |d| "#{File.join(d, 'caches', 'build-cache-1')}" }
    .select { |d| File.exist?(d) && File.directory?(d) }
    .each   { |d| puts?("\tNuking #{d}", !silent) }
    .each   { |d| FileUtils.rm_rf d } 
end

def remove_gradle_subdir?(subdir)
  name = File.basename(subdir).downcase
  targets = ["build-scan-data", "caches", "daemon", "wrapper"]
  targets.each do |t| 
    name == t ? (return true) : next 
  end
  return false
end

def annihilate_cache(gradlew, silent=false)
  puts?('Annihilating Gradle global caches...', !silent)
  `#{gradlew} cleanBuildCache`

  Dir["#{Dir.home}/.gradle/*"]
    .select { |d| File.directory? d } 
    .select { |d| remove_gradle_subdir? d }
    .each   { |d| puts?("\tAnnihilating #{d}", !silent) }
    .each   { |d| FileUtils.rm_rf d } 
end

def kill_java(silent=false)
  puts?('Killing every CLI Java instance...', !silent)
  `killall java`
end
